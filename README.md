# Mainzelliste.OSSE

## General information
Mainzelliste (see <https://bitbucket.org/medicalinformatics/mainzelliste>) is a web-based patient list and pseudonymization service. It provides a RESTful interface for client applications like electronic data capture (EDC) systems.

Mainzelliste.OSSE includes additions made to the standard Mainzelliste distribution within the Open Source Registry System for Rare Diseases in the EU (OSSE, see <https://bitbucket.org/medinfo_mainz/samply.edc.osse/wiki/Home>). These additions comprise mainly user forms adapted to the OSSE layout.

As of May 31st, 2017, the Mainzelliste-OSSE repository has moved to https://bitbucket.org/medicalinformatics/mainzelliste-osse. Please see the following help article on how to change the repository location in your working copies:

* https://help.github.com/articles/changing-a-remote-s-url/

If you have forked Mainzelliste-OSSE in Bitbucket, the fork is now linked to the new location automatically. Still, you should change the location in your local copies (usually, the origin of the fork is configured as a remote with name "upstream" when cloning from Bitbucket).